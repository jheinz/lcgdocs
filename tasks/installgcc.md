# Install `gcc` against `binutils`

### Install `binutils`

First define the binutils version to install:

```
binutilsversion=2.28
```

Create temporal directory:
```
mkdir binutils-dir
```

Define it as a prefix path:

```
export PREFIX=$PWD/binutils-dir
```

Download and untar:

```
wget https://ftp.gnu.org/gnu/binutils/binutils-${binutilsversion}.tar.gz
tar -zxf binutils-${bintuilsversion}.tar.gz
cd binutils-${bintuilsversion}
```

Create dedicated build directory:
```
mkdir obj
cd obj
```

Prepare binutils for compilation:
```
../configure --prefix=$PREFIX --enable-gold --enable-ld=default --with-system-zlib --enable-shared
```

The meaning of the configure parameters:

  - `--prefix`: Location for binaries to be placed in.
  - `--enable-gold`: Build the gold linker and install it as ld.gold (along side the default linker)
  - `--enable-ld=default`: Build the original bdf linker and install it as both ld (the default linker) and ld.bfd.
  - `--with-system-zlib`: Use the installed zlib library rather than building the included version.
  - `--enable-shared`: Build shared versions of libraries


Finally compile and install the package into the path defined by `$PREFIX`:
```
make ; make install
```

For further information follow this [link](http://www.linuxfromscratch.org/lfs/view/development/chapter06/binutils.html).

### Copy `binutils` binaries to CVMFS

- Copy the content of `$PREFIX` (`binutils-dir` in our example) to a cvmfs stratum 0 node (`cvmfs-sft`).
- Open a cvmfs transaction
- Copy the content of `$PREFIX` to a proper path inside the compilers area (a good place might be: `/cvmfs/sft.cern.ch/lcg/contrib/binutils/<version>`)
- Modify the path in all `*.la` files (inside `lib`) to point to the correct path (rather than the path inside the build node where the package was compiled)
- Publish transaction


### Install `gcc`

First, start defining the gcc version to install:

```
gccversion="6.2.0"
```

Create a temporal directory for the binaries:

```
mkdir gccinstall
export PREFIX=$PWD/gccinstall
```

Download gcc source (tar.gz) and untar it:

```
wget http://mirror.switch.ch/ftp/mirror/gnu/gcc/gcc-${gccversion}/gcc-${gccversion}.tar.gz
tar -xf gcc-${gccversion}.tar.gz
cd gcc-${gccversion}
```

As for `binutils`, `gcc` is recommended to be built in a different directory from source:

```
mkdir obj
```

Download the `gcc` prerequisites:

```
contrib/download_prerequisites
```

To use an external binutils, add it to the path and add some extra configuration options:

```
cd obj/
export PATH=$BINUTILS_PATH/bin:$PATH
./configure --prefix=$PREFIX -with-system-zlib --disable-multilib --enable-languages=all --with-gnu-ld --with-ld=$BINUTILS_PATH/bin/ld --with-gnu-as --with-as=$BINUTILS_PATH/bin/as
```

where:

- `$BINUTILS_PATH`: Point to the binutils external path (should be cvmfs in our case).
- `--prefix`: Location for binaries to be placed in.
- `--with-system-zlib`: Use the installed zlib library rather than building the included version.
- `--disable-multilib`: Do not build multiple target libraries.
- `--enable-languages=all`: Compilers and their runtime libraries for all available languages are built.
- `--with-gnu-ld`: Specify that the compiler should assume that the assembler it finds is the GNU linker.
- `--with-ld=$BINUTILS_PATH/bin/ld`: Since we will have more than one linker in `$PATH`, we need to specify that the compiler should use the linker pointed to by `$BINUTILS_PATH/bin/ld`.
- `-with-gnu-as`: Same as `--with-gnu-ld` but for the assambler.
- `--with-as=$BINUTILS_PATH/bin/as`: Same as `--with-ld` but for the assambler.

Finally, compiler and install:

```
make; make -jN install
```

where *N* is number of cores to use.


### Copy `gcc` binaries to CVMFS

Follow the same steps mentioned for `binutils`:

- Copy the content of `$PREFIX` (`gccinstall` in our example) to a cvmfs stratum 0 node (`cvmfs-sft`).
- Open a cvmfs transaction
- Copy the content of `$PREFIX` to a proper path inside the compilers area (a good place might be: `/cvmfs/sft.cern.ch/lcg/contrib/binutils/<version>`)
- Redefine every path in `.la` files, a shorcut to do so could be:

    ```
    find . -iname "*.la*" | xargs sed -i 's@/path/where/was/locally/installed@/cvmfs/new/path/ARCH-PLATFORM@g'
    ```

    > **Beware that...**
    > `sed -i` will find and replace all the occurrences of the specified pattern and save the files
    > found by `find . -iname "*.la*"`. It may be helpful to check which changes will be applied before modifying
    > all the files just using the same command but without `-i`:
    > ```
    > find . -iname "*.la*" | xargs sed 's@/path/where/was/locally/installed@/cvmfs/new/path/ARCH-PLATFORM@g'
    > ```

- Publish transaction
