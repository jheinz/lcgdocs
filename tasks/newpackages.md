# Adding new system packages

Usually, the obvious way to add packages to the system layer is to ask [Andrea Valassi](mailto:Andrea.Valassi@cern.ch) to add them to the [HEP_OSlibs](https://gitlab.cern.ch/linuxsupport/rpms/HEP_OSlibs). Since the HEP_OSlibs are a bit too big for some use cases and a quick solution is needed, here's an explanation how to add packages to the Docker images (used for nightly builds) or the puppetized VMs (used for releases).

## Docker

In Docker, there's a `Dockerfile` per operating system. Since *SLC 6* and *CentOS 7* are provided by IT, we extend the functionality by adding additional packages and creating the necessary folders. For *Ubuntu* and *Fedora* we customize the entire system to our needs.

As far as naming for Ubuntu images goes:

 - Scientific Linux 6 is called `slc6`
    - Based on an image provided by CERN IT
    - Uses `HEP_OSlibs`
 - CentOS 7 is called `cc7` (or `centos7` as image)
    - Based on an image provided by CERN IT
    - Uses `HEP_OSlibs`
 - Fedora 27 is called `fedora27`
    - No `HEP_OSlibs` (lightweight image)
 - Fedora 28 is called `fedora28`
    - No `HEP_OSlibs` (lightweight image)
 - Fedora 29 is called `fedora29`
    - No `HEP_OSlibs` (lightweight image)
 - Fedora 30 is called `fedora30`
    - No `HEP_OSlibs` (lightweight image)
 - Ubuntu 16.04 is called `ubuntu16`
    - LTS release (long term support)
    - Uses `HEP_OSlibs` 
 - Ubuntu 18.04 is called `ubuntu18`
    - LTS release (long term support)
    - No `HEP_OSlibs` (lightweight image)
 - Ubuntu 18.10 is called `ubuntu1810`
    - Regular Ubuntu release (short support)
    - Uses `HEP_OSlibs`
 - Ubuntu 19.04 is called `ubuntu19`, a regular release, 
    - Regular Ubuntu release (short support)
    - No `HEP_OSlibs` (lightweight image)

In the [Container Registry](https://gitlab.cern.ch/sft/docker/container_registry) you can find each image with two different name:

 - The old approach is to keep all operating systems under the same name and use the *tag* as a means to tell which OS the image refers to, e.g. `gitlab-registry.cern.ch/sft/docker:lcg-cc7`
 - As a new (parallel to the old) approach there's a naming scheme that uses the *tag* only for versioning and not to specify the OS: e.g. `gitlab-registry.cern.ch/sft/docker/centos7:latest`

The new naming approach drops the prefix `lcg-` for all images and renames `cc7` to `centos7`. Currently, it is not yet used in our scripts. Still this way is more concise with the Docker way of naming and tagging images.

### Preconditions

 - You need to have at least the role `Developer` in the project `sft/docker`: [https://gitlab.cern.ch/sft/docker/project_members](https://gitlab.cern.ch/sft/docker/project_members)
 - Make `sftnight` a `Developer` for one day (via access expiration day) as well to allow a push to the Docker registry
 - Login to the Docker registry in GitLab if you want to push to the registry manually (without the scripts): `docker login gitlab-registry.cern.ch`. This creates a local file that can be removed with `docker logout gitlab-registry.cern.ch`. On the build servers, the login file is already available in the shared AFS home of `sftnight`.

### Step-by-step

 1. Clone the `sft/docker` repository. The relevant `Dockerfile`s are in the `builder` sub-folder.
 2. Find out the correct name of the package or library you want to add for each platform. Usually there are at max two names: One for Debian/Ubuntu and one for RedHat/SLC/CentOS/Fedora.
 3. Add the package name to the installation command
    - For `slc6`, `cc7`, `fedora27`, `fedora28`, `fedora29`, `fedora30`, `ubuntu16` and `ubuntu1810` directly in the `Dockerfile` (don't forget the line break `\`)
    - For `ubuntu18` in a file called `minimal.packages`
    - For `ubuntu19` in a file called `packages.txt`
 4. Add, commit and push your changes to the git repository with a comment that explains why the new package is needed.
 5. Add *sftnight* to the GitLab project `sft/docker` as *Developer* so it is allowed to push images to the GitLab container registry.
 6. Run the job `lcg_build_docker_images` in [Jenkins](https://epsft-jenkins.cern.ch/job/lcg_build_docker_images/). This command provides ...
    - Optional backup of old images (in the old naming scheme, with the new naming there is no overwrite)
    - Remove the local image to work on a clean slate
    - Build and tag the Docker image based on the selected `Dockerfile`
    - Verify that all necessary software is present
    - Push the new image to the GitLab container registry
    - Tag the image with new names
    - Remove the image locally again to reduce the impact on the local volume

If you want to execute step 6 manually, have a look at the annotated script `builder/build.sh` that is executed by Jenkins.

## Puppet

Puppet is a configuration management tool that is used by CERN to manage the SLC 6 and CentOS 7 VMs.
Ubuntu, Fedora and other Linux flavors are not supported by CERN IT.
Each Puppet hostgroup has a GitLab repository in the `ai` group.
Our repository is called [it-puppet-hostgroup-lcgapp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgapp).

In Puppet, you describe the desired state of your target system (e.g. a CentOS 7 VM).
For example you don't tell Puppet *"Install package XY!"* but instead *"Ensure that package XY is installed!"*

Puppet runs once per hour on each VM and verifies that the target system is in the described state.
If not, it takes the necessary steps to reach this state, e.g. install a missing package.

You can lookup the current state of all puppetized VMs in a web application called *Foreman*.
It runs at CERN on a server called [judy](https://judy.cern.ch) (requires CERN certificates for TLS)

The most relevant branches in the git repository are:
 - `master` refers to the environment *Production*
 - `qa` refers to the environment *QA*

We use both environments for our build nodes.
The recommended environment is *Production*.

A manual reload of the latest configuration on a puppetized VM can be forced - for impatient developers :)
```
puppet agent -tv
```

### Preconditions

 - You need to be in the following eGroups:
    - [VOBox-Responsible-SFT](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=189044)
    - [ai-admins](https://e-groups.cern.ch/e-groups/Egroup.do?egroupId=326041)
 - You are automatically assigend as member of the GitLab project [it-puppet-hostgroup-lcgapp](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgapp/project_members) as a *Developer* if you are in the eGroup ai-admins.

### Step-by-step

 1. Find out which hostgroup and which environment you want to modify, e.g. via [Foreman](https://judy.cern.ch/hostgroups)
 2. Clone the git repository
    - Checkout the `qa` branch if necessary (if you want to change QA instead of Production)
 3. Find out the correct name of the missing package for CentOS/RedHat releases. There's a difference in naming across different Linux flavors.
 4. Find your hostgroup's specification file, e.g. `code/manifests/x86_64/build/cc7/docker.pp` for the hostgroup `lcgapp/x86_64/build/cc7/docker`
 5. Use the Puppet [resource type package](https://puppet.com/docs/puppet/5.5/types/package.html) to ensure the presence of your desired package `packageXY`:
    ```
    package{ "packageXY" : 
        ensure => present
    } 
    ```
    You can also pass a list of packages:
    ```
    package{ ['packageA', 'packageB', 'packageC'] :
        ensure => present
    } 
    ```
 6. Add, commit and push your changes with a comment that explains why the new package has been added
 7. Repeat steps 3 to 5 for another branch if your target machines are split across multiple environments, e.g. first add the package in the `master` branch and then in the `qa` branch
 8. Wait for an hour or force a reload with the command mentioned above.
 9. Check the report for an effected node in Foreman to make sure that no errors occurred

