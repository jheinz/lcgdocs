# Copy to EOS

* Go to lxplus using you account: `ssh lxplus`
* Setup the environment variable: `EOS_MGM_URL=root://eosuser.cern.ch`
* Go to the path where the files are, and copy them using `xrdcp`:

```
xrdcp -f <File_name> root://eosuser.cern.ch/path/in/eos/<File_name>
```

For further info: [EOS quick tutorial for beginners](https://cern.service-now.com/service-portal/article.do?n=KB0001998)


## Copy new source tarfiles to EOS

```
xrdcp -f <File_name> root://eosuser.cern.ch//eos/project/l/lcg/www/lcgpackages/tarFiles/sources/<File_name>
```

The common name convention for tarfiles is:

```
Name-1.0.0.tar
```

Beware that generators packages are all included in a special folder: *MCGenerators*, so the `xrdcp` command
should be instead:

```
xrdcp -f <File_name> root://eosuser.cern.ch//eos/project/l/lcg/www/lcgpackages/tarFiles/sources/MCGenerators/<File_name>
```
