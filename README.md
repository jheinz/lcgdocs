# EP-SFT: Internal Documentation for SPI related activities

These pages are a collection of HOWTO's documenting the operations required to run the LCG infrastructure.

The topics currently available are:

### LCG machine managemant
* [Openstack](tasks/openstack.html)
* [Puppet configuration](tasks/puppet.html)
* [Dockers in LCG](tasks/dockers-in-lcg.html)
* [Adding hosts to jenkins infrastructure](tasks/newhosts.html)
* [Adding CERN user accounts](tasks/newcernuser.html)
* [Adding new system packages to Docker and Puppet](tasks/newpackages.html)

### Common LCG tasks

* [Copy to EOS](tasks/copytoeos.html)
* [Cleaning a workspace](tasks/cleanup.html)
* [Install a new `gcc` compiler](tasks/installgcc.html)


