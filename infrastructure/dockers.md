# Use of DOCKERS in LCG nightlies and releases



## Introduction


Docker is the world’s leading containerisation platform designed to make it easier to create, deploy and run various applications by using containers. [Docker containers ](https://docs.docker.com/get-started/) wrap up a piece of software into a complete file system that contains everything the software needs to run.

A container basically consists of an entire runtime environment, which includes an application with all its dependencies, like libraries and other binaries, and even the configuration files needed to run it, all bundled into one package. Hence, by containerising the differences in operating system distributions, the underlying infrastructure is abstracted away. Containers encapsulate all the discrete components of application logic which are provisioned, that too with the minimal resources needed to do their job.
[check for more information here ..](https://opensourceforu.com/2017/02/docker-favourite-devops-world/).

## Virtual Machines (VM) configuration (puppet) for using docker images


In LCG nightlies, dockers are used in order to optimize the use of the machines and minimize maintenance of them. 

A set of virtual machines (running cc7, slc6 and ubuntu16) are defined in openstack and their initial configuration is done through puppet service.

The scripts that are launched using puppet services are defined in a tree structure in GitLab in [“it-puppet-hostgroup-lcgapp”](https://gitlab.cern.ch/ai/it-puppet-hostgroup-lcgapp).


The docker images are prepared and stored in Gitlab in the project [sft/docker](https://itlab.gcern.ch/sft/docker)

## Using dockers through Jenkings for LCG nightlies

The use of dockers in Jenkins is achieved by running the docker image directly in the “excecute shell” tab of the Build session of the jenkins task. The specific docker image to be downloaded from gitlab and used is indicated with the Jenkins parameter $LABEL which is defined in the “Matrix Configuration” session of the jenkins task. In order to avoid confusion with the jenkins ptasks that are not using dockers, here the the $LABEL is used to identify the docker image while the physical node can run any type of operating system.

In a few words, this script is running the adequate docker image and executes the “runall-docker.sh” in the  lcgjenkins subdirectory by passing three parameters:   BUILDTYPE, COMPILER and LCG_VERSION

The log files are transmitted to jenkins and to CDASH and so far there are no instructions on how to monitor the process as it was usually done by logging into the machine where the jenkins job were executed. Nevertheless, we provide a possibility to access the docker in case the compilation was failing in order to perform in depth investigations in case the information which is made available through the log files is not enough.

All files which are kept locally in the docker during the build of the packages are copied in tar files to the “master” node following the completion of each package build. Once all packages are built, the docker image is deleted and the tar files are copied from the “master” node to the “/eos” space. The compilation results are made available to the dedicated local filesystem mounted in /mnt and consequently mounted by the dockerimage.

In case the build was successful, the packages are transferred to the /eos/... space and  the docker image is deleted. In case of failure, it is possible to execute rhe "rundocker.sh" script which will generate a docker image with the same configuration with the one that has been used to produce the compilation results for further investigations. For example, the script  located in "/mnt/build/jenkins/workspace/lcg_experimental_archdocker/BUILDTYPE/Release/COMPILER/gcc7binutils/LABEL/lcg_docker_cc7/rundocker.sh" will run a preconfigured docker image for a centos7 platform using the g77-binutils compiler.





## LCG Releases in Dockers

LCG Releases are published in Dockers starting from Release LCG-93. The docker containers are made available to the users in EOS and located in: 

/eos/project/l/lcg/www/lcgpackages/docker/releases/

The procedure to generate those docker LCG releases is [defined in the jenkins](https://epsft-jenkins.cern.ch/) script *lcg_release_dockerimage* and it consists in the following steps

1. Create a tar archive file with all/selected packages of the specified release
2. Create a platform specific docker image and import the previously generated tar archive
3. Export the docker image in tar archive or repository
4. Copy the exported tar file containing the docker image in EOS
5. Delete the docker image of the docker container

The configuration matrix of the script defines the platforms (centos7 slc6 ubuntu16), the compilers (gcc62 gcc7 clang60 native) and the "modes" (opt,dbg) and the script is executed for each combination of them.


More in detail, the creation of LCG Releases in dockers deploys the use of a series of scripts:

* in [Jenkins](https://epsft-jenkins.cern.ch/):
	* [lcg_release_dockerimage](https://epsft-jenkins.cern.ch/job/lcg_release_dockerimage/)
* In [gitlab](https://gitlab.cern.ch/sft/):
	* [lcgtools](https://gitlab.cern.ch/sft/lcgjenkins/blob/master/tools/lcgtools)
	* [dockerbuild](https://gitlab.cern.ch/sft/docker/blob/lcgdocker/dockerbuild)
	* [lcg.sh](https://gitlab.cern.ch/sft/docker/blob/lcgdocker/release/scripts/lcg.sh)
	* [install_contrib.sh](https://gitlab.cern.ch/sft/docker/blob/lcgdocker/release/scripts/install_contrib.sh)
	* [cc7 Dockerfile](https://gitlab.cern.ch/sft/docker/blob/lcgdocker/release/cc7/Dockerfile)
	* [slc6 Dockerfile](https://gitlab.cern.ch/sft/docker/blob/lcgdocker/release/ubuntu16/Dockerfile)
	* [ubuntu16 Dockerfile](https://gitlab.cern.ch/sft/docker/blob/lcgdocker/release/slc6/Dockerfile)

An explanation of each of those scripts is given below:
------------------------------------------------------------------------------------------------


## lcgtools (python script, a wrapper of docker command):
The first parameter of the script is the command to be executed which can be:

**tar
depends
isdependency**

### other parameters:
#### help
	"Generate single archive file contaning release or subset of release"
#### -v, --verbose
#### --nodeps
	"Don't resolve dependencies"
#### -source, choices=['eos', 'cvmfs', 'eosweb'], default='eos', 
	eos - take from eosclient [default/recommended]; 
	cvmfs - take from CVMFS [slow]; e
	eosweb - take from eos web interface [portable] "
#### --source_path
	"Define custom path to the corresponding source"
#### --tarname, default="LCG.tar"
	"Name of archive. Compression type is resolved through name extension. [.tar,.tar.gz]"
#### --lcg_version, default="LCG_93"
	"Version of LCG. 'last' resolves automatically the latest LCG version"
#### --lcg_source
	"Path to release file. By default it takes the path to eos web interface"
#### --platform', default="x86_64-centos7-gcc62-opt"
#### packages
	"List of all packages to include in the tar file. 'all' to take everything"

The commands used to display the dependency graph with the commands “ depends” and "isdependency" are not explained here.


### Description:
The “lcgtools” python script is used to generate single “.tar” archive file containing all the packages of an LCG release or a subset packages of an LCG release specified by the “packages” parameter. This tar file may then be used to import and install those packages in a platform specific docker image.

### Example:
./lcgjenkins/tools/lcgtools tar -v --platform x86_64-cc7gcc7-dbg --lcg_version LCG_93 all

------------------------------------------------------------------------------------------------


## dockerbuild:

### Input parameters/commands:
ls  - list all possible images to build
build [--local-user] [-t TAG] [docker build args] IMG    - build specified image
get_command [--local-user] [-t TAG] IMG - print the command to build
export --tar|--registry IMAGE_NAME DEST

### Description:
The dockerbuild script disposes of two main comands:
build: used to create a new docker image for a specific platform containing all the packages in the archive “.tar” file created using the “lcgtools” script. 
Export: used to export the docker image to tar archive (if –tar is specified as a  parameter) or to a registry  (if –registry is specified as a  parameter)


### examples: 
./dockerbuild ls
./dockerbuild get_command  -t release/cc7:LCG_93 release/cc7
./dockerbuild build -t release/cc7:LCG_93 release/cc7
./dockerbuild export --tar release/cc7:LCG_93  LCG_93-docker-cc7.tar

------------------------------------------------------------------------------------------------
## install_contrib.sh:
	Install platform specific compilers

------------------------------------------------------------------------------------------------
## lcg.sh:
	Define platform specific environmental variables
------------------------------------------------------------------------------------------------



## Dockerfile:

### Input parameters:  
tarfile [=LCG.tar]. 
	The name of the archive “.tar”  containing  all packages from the specified release. 
	This “.tar” archive has been produced using the “lcgtools” script”

### Description:
Dockerfile will create a new “docker image” for a specific platform [ cc7,  slc6,  ubuntu16]. The image will contain 
	a predefined directory structure similar to the one used in the releases in “/cvmfs”, 
	the compiler [using the script “install_contrib.sh”] which is the same used to compile each individual package of the release and also 
	the environment variables [using the script “lcg.sh”]. 
The platform is chosen by selecting the appropriate path for the script “Dockerfile”.
The Dockerfile script is used by the “dockerbuild” script.



## Usefull links

https://docs.docker.com/get-started/
	docker containers information
https://gitlab.gcern.ch/sft/docker
	Pre-created platform specific docker images specially configured for use in nightlies
https://epsft-jenkins.cern.ch/
	SFT jenkins web site
https://epsft-jenkins.cern.ch/view/LCG%20Externals/job/lcg_release_dockerimage/
	Jenkins LCG docker Release creation script
https://gitlab.cern.ch/sft/
	SFT gitlab reositories
https://gitlab.cern.ch/sft/lcgjenkins/tree/master/tools
	Tools (scripts) used for the LCG docker release
https://gitlab.cern.ch/sft/docker/tree/lcgdocker/release (branch lcgdocker)
	Additional tools (scripts) used for the LCG docker release (docker image)

