# Adding hosts to the jenkins infrastructure

### Docker hosts

1. Follow the steps in [Create a new virtual machine (docker host)](openstack.md) to create a new OpenStack virtual machine.
2. Don't forget to create the new volume and attach it.
3. Add a ssh key to access using `sftnight` credentials.
  * From any existing node, use `scp` to copy it over the same path in the new vm
4. Add it as a new Jenkins node:
    * Go to [Jenkins nodes](https://epsft-jenkins.cern.ch/computer)
    * Click on *New node*
    * Add a node name
    * Copy it from an existing one, i.e. `lcgapp-centos7-x86-64-31`
    * Modify the argument in the *Launch command* to the correct hostname
    * Set up the correct bunch of labels
