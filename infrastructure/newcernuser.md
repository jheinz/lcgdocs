# Create an account for an existing CERN user

To add a new CERN AFS user account to a computer you need to use the following command:

```
useraddcern <cern_username_account>
```

Prerequisites:
  - A CERN AFS account present/visible in the LDAP system (to look for an existing account: [phonebook](https://phonebook.cern.ch/phonebook/#))
  - Root privileges

