# LCG operations inside Jenkins

All tasks from the LCG operations should be under _LCG Externals_.
The tasks are unfortunately stored in two places: jenkins itself and [lcgjenkins](lcgjenkins) repository. All the jobs should probably
be stored inside the repository. That could improve the reusability of the code as well as better history management.


## Jenkins task description
This section contains basic information of the tasks that are used daily as well as information about task that might be
deleted in the future.

### Nightlies
| Job name | Description | Type | Incremental |
| -------- | ----------- | ---- | ----------- |
| *lcg_experimental_archdocker* | Task that compiles the *experimental* toolchain **without** installation to EOS or CVMFS. | Docker | Yes |
| *lcg_experimental* | Task that compiles the *experimental* toolchain **without** installation to EOS or CVMFS | Native | Yes |
| *lcg_experimental_full* | Task that compiles the *experimental* toolchain **without** installation to EOS or CVMFS. The compilation is always from scratch| Native | No | 
| | | | |
| *lcg_ext_dev3_archdocker* | Task that compiles the *dev3* toolchain with installation to EOS or CVMFS | Docker | Yes |
| *lcg_ext_dev4_archdocker* | Task that compiles the *dev4* toolchain with installation to EOS or CVMFS | Docker | Yes |
| *lcg_ext_dev3python3_archdocker* | Task that compiles the *dev3python3* toolchain with installation to EOS or CVMFS | Docker | Yes |
| | | | |
| *lcg_post_build* | Installation manager task. Created to release the build machine, otherwise it was waiting for cvmfs | --- | --- |

Jobs not used anymore:
* *lcg_ext_dev3_archdocker* - blocked build machine until the cvmfs installed
* *lcg_ext_dev4_archdocker* - see previous
* *lcg_ext_dev4python3_archdocker* - see previous
* *lcg_ext_dev4python3_archdocker_nonblocking* - Using dev3python3 instead
* *lcg_ext_dev3arch* - tries to build everything with AVX support 
* *lcg_ext_dev3_archdocker_nonblocking_old*
* *lcg_ext_dev3_new*

Type explanation:
* Docker - the task is using docker to compile on different platforms
* Native - the task is using target os to compile the exact platform

##### Little more about *lcg_post_build*
The installation is done *one at the time* and it is managed by jenkins.
This task was created in order to not block the build machine, as previously the build machine was waiting for cvmfs to
be release, waisting the resources ( the machine was blocked sometimes for 6 or even more hours ). 
This task is getting all the necessary information via the parameters and the packages are stored already in EOS.
The task runs following steps:
1. *lcg_cvmfs_install* - install to cvmfs
2. *lcg_dbpublish* - update information about the installations in lcginfo
3. *lcg_test_archdocker* - test the installation using LCGTest

Currently the jobs are terminated after 9pm, as it is already useless and the next nightlies will compile shortly. There
is probably a bug or not well defined behavior that causes triggering *lcg_cvmfs_install* although the task is running
after the deadline. The current solution to this problem is a check on *lcg_cvmfs_install* if the day is correct. This
behavior is by default off and is activated by passing "true" in *IGNORE_PREVIOUS_DAY* parameter ( by default passed to
*lcg_post_install*).

### Release
The order of task execution during the release
1. *lcg_release_tar* - THE main task to build the release
2. *lcg_cvmfs_install* - MANUAL trigger the installation to CVMFS - be sure that what compiled is correct
3. *complete_rpms_release_actions* - Updates the rpm database inside EOS
4. *lcg_release_dockerimage* - publish docker image with the release ( [more information](../infrastructure/dockers) )

Other jobs:
* *lcg_release_latest* - Takes the current dev3, uses specified in job ROOT and installs it to the release area.
* *lcg_release_tar_archdocker* - Prepared job for the release compilation inside docker - needs to be tested !!
* *lcg_remove_release* - Remove safely a release - if package use only by this release it will be also removed

### Common between nightlies and release

* *lcg_cvmfs_install* - install to cvmfs
* *lcg_cvmfs_emergency_install* - because *lcg_cvmfs_install* is used by everything, it might be long before installing next item - this task will be executed after the currently executed task finishes on CVMFS. Triggered manually.
* *lcg_dbpublish* - publish changes in [LCGInfo](http://lcginfo.cern.ch/)
* *lcg_test_archdocker* - source the view from cvmfs inside docker and test it with LCGTest
* *lcg_single_test* - source the view from cvmfs on native machine and test it with LCGTest


### Contrib
Release the compilers
* *lcg_contrib_release* - Build, copy tarball to EOS, install to cvmfs and create RPM ([more info](contrib))
* *lcg_contrib_cvmfs_install* - Triggered automatically. Installs the compiler to cvmfs
* *lcg_contrib_create_rpm* - Triggered automatically. Creates rpms and publishes to RPM database.

### Other operations
| Job name | Trigger | Machine/Label | Description | Should it work? | Can I kill it? | Can it get stuck? | Status |
| -------- | ------- | ------------- | ----------- | --------------- | -------------- | ----------------- | ------ |
| *lcg-clean-builds* | H(0-20) 7,14,20 \* \* \* | All build nodes | Clean */build/jenkins/workspace* directory. Deletes directories with **controlfile** older then 2 days | Yes, if not, nodes will run out-of-space | Yes, it is running 3x per day | Yes, usually if a machine will be disabled, the task will wait unnecessarily for it, in this case it should be killed. | On |
| *lcg_clean_cvmfs_nightlies* | H(1-10) 18 \* \* \* | cvmfs-sft, cvmfs-sft-nightlies | Delete all packages inside */cvmfs/sft-nightlies.cern.ch/lcg/nightlies/$SLOT/$TOMORROW* | Yes, if not the nighlies will contain also outdated packages | You should not, it might leave open cvmfs transaction | It should not | On |
| *lcg_cleanMaster* | H(1-10) 0 \* \* \* | master | Remove all tarballs from jenkins master node | Yes, if not master might run out-of-space | Probably yes, it will run again next day | It should not | On |
| *lcg_controlfile_check* | 0 3 H/3 * * | All build nodes | Report all projects that do not create *controlfile*. Needed in order to remove | It does not have to work | Yes | Yes, the same as *lcg-clean-builds* | Disabled |
| *lcg_cvmfs_gc_nightlies* | H(20-30) 18 * * * | cvmfs-sft-nightlies | Run manually Garbage collecting in CVMFS sft-nightlies server | Yes, more information in [cvmfs](cvmfs) section | Yes | Yes, if the deadline in jenkins will not work | On |
| *lcg-check-disk-space* | | | Send mail if short on disk space - currently used grafana | | Yes | Yes, the same as *lcg-clean-builds* | On |
| *lcg_clear_task_workspace* | | Selected build nodes | Delete workspace of a particular project from selected build machines | | | | Off |
| *lcgdocs-build* | H H(22-23) * * * and gitlab trigger| slc6-physical2 | Create this gitbook from gitlab.cern.ch/sft/lcgdocs repository | Yes | Yes | No | On | 

### Other

### Depricated
* *lcg_cleanNightliesFromAFS* - AFS not used
* *lcg_export_container* - the container is no more pushed to EOS
* *lcg_ext_initdocker* - working dockers in nightlies


### Unknown status
#### Used frequently, but no idea why
* *lcg-clean-lcgsoftDB* - old lcgsoft [ not required? ]
* *lcg_hadoop_context* 
* *lcg_hepmc_nightly* 
* *lcg_lastupdate_file* - Creates an empty cvmfs transaction?
* *lcg_pythia8_nightly*


#### Not used for very long time (obsolete?)
* *lcg_afs_install* - installation to afs [ not done anymore? ]
* *lcg_ext_BE_tests* 
* *lcg_ext_dev3atlas*
* *lcg_gaudi_build*
* *lcg_generators_build*
* *lcg_generators_install*
* *lcg_generators_install_afs*
* *lcg_pythia8_nightly*
* *lcg_release_dbg* - Using only lcg_release_tar to release opt and dbg

