
# Introduction of new compiler

The actuall set of used set of compilers are defined in LCGCmake: _cmake/toolchain/LCG_contrib.cmake_

```
LCG_external_package(binutils          2.30                                     )
LCG_external_package(gcc              6.2.0                                     )
LCG_external_package(gcc              7.3.0                                     )
LCG_external_package(gcc              8.1.0                                     )
LCG_external_package(clang            6.0.0           gcc=7.3.0                 )
```

After changing and committing the changes, the *lcg_contrib_release* should be launched.

Note to clang compilation:
The clang has a gcc dependency, as it requires the libstdc++. To be able to compile C++17 with clang, we need gcc 7.3 or
greater. Currently the dependency is bound to gcc-7.3.0.

# **lcg_contrib_releas** - Release of the contribe

## *jk-runtime-compiler.sh*

1. Source the compiler passed with the COMPILER env variable
2. Source Python (slc6) - llvm requires python >=2.7, where on slc6 there is python 2.6 (currently surpassed by Jinja2 that also includes Python) 
3. Source Jinja2 - currently it is not used, but the future improvement of the lcg contrib might need creating setup.sh file during lcgcmake build to pack it into tarball
4. Run _lcgcmake-build-compiler.cmake_ script with ctest
5. Copy tarballs from build node to jenkins master node
6. Produce information required for next jenkins step (_jenkins.properties_)

## **LCG-Master-to-EOS**

This step is reused from the standard LCG procedure.
Copy tarballs from jenkins master node to EOS

## **lcg_contrib_cvmfs_install**

This task does not require to relaunch the *lcg_release_contrib*. It can be launch separately in order to reinstall the
compilers, recreate the _setup.{sh,csh}_ files and redo the links. 

1. Install tarballs to cvmfs area with _lcginstall.py_
2. Get LCG_contrib from EOS
3. Create summary ( _LCG_contrib_x86_64-{slc6,centos7}.txt_ )
4. Create _setup.sh_ file - for every compiler in *LCG_contrib.txt* file
5. Create links to contrib area - create compatible layout inside contrib area 

### *create_setup.py*

Creates generic, relocatable _setup.{sh,csh}_ script based only on buildinfo with Jinja2 template engine.

Template search order:
1. `<pkg_name>.<shell>.in`
2. `common.<shell>.in`

The `common.<shell>.in` is sourcing all dependencies of this package, as well as exporting some common variables: PATH,
MANPATH, LD_LIBRARY_PATH.
The `<pkg_name>.<shell>.in` should contain package specific environmental variables, e.g. `gcc.sh.in` is exporting CXX, CC
and FC. In order to include the common setup, at the beginning of the pkg specific script, this line should be added:
`include "common.sh.in"` [see gcc.sh.in](https://gitlab.cern.ch/sft/lcgjenkins/blob/compilers/compilers/source_env/envs/gcc.sh.in).
The information about the name, version and hash are stored in `__PKG_NAME`, `__PKG_VERSION`, `__PKG_HASH` variables (not
exported!!).

The current script is used only for contrib, but it can be also used with all other LCG packages. It can be compared
with LCGenv, but this solution does not require Python for the runtime.

### *link_contrib.sh*

Creates compatible contrib layout. This step is optional.
Many projects and experiments were using contrib and in order to not break the existing script it is required to create
a link to from the release area, where the new hashed version of contrib are installed, to the contrib area.
To be sure that the links will be still fine even by changing the root directory from /cvmfs/sft.cern.ch to other, the
link is a relative path to that directory.

Functions:
- relative_path - Returns relative path from $1 to $2
- relln - Creates a relative link $1 to $2. It will replace the link if exists and create the parent directories $1 if not existing.  

There is 4 cases for the link structure:
1. GCC < 7
2. GCC >= 7
3. Clang - llvm is created to stay back compatible, but clang should be used instead.
4. Default

## **lcg_contrib_create_rpm**

This step can run independently - it will recreate the contrib RPMs based on last successful contrib build.

1. Source Jinja2
2. Create directory structure for RPMS: rpmbuild/{BUILD,BUILDROOT,RPMS,SOURCES,SPECS,SRPMS}
3. Download *LCG_contrib_${platform}.txt* from EOS
4. Download package tarballs that LCG_contrib contains
5. Create spec file with *create_LCG_contrib_SPEC.py*
6. Run `rpmbuild` for all packages
7. Copy RPMs to EOS
8. Update RPM repository

### *create_LCG_contrib_SPEC.py*

The generation of SPEC files follows the same idea as creating setup files. 
The variables available at SPEC file generation are a set of predefined options passed to the command and all variables
from buildinfo file.
The generated specfile takes as an input package tarfile. This allows us to run the generation of RPMs on any machine
that has python >= 2.7 installed, this means that it will not work on slc6. 
The installation steps of the specfile is untaring the package and if required, create the setupfile.

