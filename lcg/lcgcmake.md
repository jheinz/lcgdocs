# LCGCMake

## Special packages

### Cuda
The Cuda toolkit is installed using pre-made binaries that can be downloaded from [CUDA toolkit archive](https://developer.nvidia.com/cuda-toolkit-archive) for which you need to register as developer. The binaries *run file(local)* have been copied to our EOS source area. The installtion is a easy as just running the downloaded self extacting script

    INSTALL_COMMAND <SOURCE_DIR>/cuda_${cuda_full_version}_linux 
                  --silent         # disable interactive prompts
                  --verbose        # create verbose log file
                  --override       # override compiler version checks
                  --toolkit        # install CUDA Toolkit
                  --toolkitpath=<INSTALL_DIR>

                  

### Tensorflow

*The most recent version of tensorflow (1.12.0) has been installed using the already made wheel packages and not following the instructions building from sources* 

#### Installing already build binarires (on Linux and MacOS systems)
The [tensorflow installation page](https://www.tensorflow.org/install/pip) provides detailed instructions on the installation process using **pip**. At the end of the page there is a list of pre-made wheel files for different platforms and python versions. We have copied the relevant files to our EOS area without any change.
The installation becomes a simple pip install

    pip install --no-deps --prefix=<INSTALL_DIR> <SOURCE_DIR>/tensorflow${packsuffix}-${tensorflow_native_version}-${pyversion}-${pylibversion}-${os}_x86_64.whl


#### Building from sources
Tensorflow is using *bazel* build system, that does not allow for an very easy build.
Problems:
* It requires Python 2.7 at build time - therefore we cannot use the python from LCG as we can have only one python at the same time. The python is compiled from scratch and pointed to the directory with **PYTHON_BIN_PATH**.
* Bazel tries to build in a completely isolated environment - the LD_LIBRARY_PATH and gcc flags are not passed - for this there is many patches, patching also basel itself to somehow remove this behaviour.
* Bazel takes care of all dependencies - this does patching the sources quite hard, for now the script is exploiting the behaviour that after failure it stops and by the next relaunch it takes the already cached files. Case with protobuf.
* Default /usr/bin/gcc and crosstool not the easiest thing in the world

In general: bazel is not package manager friendly

The build script is in [lcgjenkins](https://gitlab.cern.ch/sft/lcgjenkins/blob/master/build_recipes/build_tensorflow.sh) repository.

HowToBuild:
Because of the mentioned problems with compilers, there is a dedicated machine `lcgapp-slc6-tensorflow-buildmachine`
that contains preinstalled newer binutils.
The compiler still needs to be sources.

1. Build sqlite & python
2. Install pip, numpy, nltk, enum34, mock, virtualenv
3. Build bazel ( most likely the version of tensorflow is bound to a bazel version )
4. Get tensorflow
5. Export variables required for Tensorflow configuration
6. Build tensorflow - targets: //tensorflow/tools/pip_package:build_pip_package //tensorflow:libtensorflow.so //tensorflow:libtensorflow_cc.so
7. Pack tensorflow to python wheel: tensorflow/tools/pip_package/build_pip_package
8. Build tensorboard

The output that we are expecting is a python wheel package, that we put to EOS.

