Summary

* [About this documentation](README.md)

### LCG
* [LCG operations](lcg/ops.md)
* [LCGCmake](lcg/lcgcmake.md)
* [Jenkins](lcg/jenkins.md)
* [LCGjenkins](lcg/lcgjenkins.md)
* [EOS](lcg/eos.md)
* [CVMFS](lcg/cvmfs.md)
* [LCG contrib](lcg/contrib.md)

### LCG machine managemant
* [Openstack](infrastructure/openstack.md)
* [Puppet configuration](infrastructure/puppet.md)
* [Dockers in LCG](infrastructure/dockers.md)
* [Adding hosts to jenkins infrastructure](infrastructure/newhosts.md)
* [Adding CERN user accounts](infrastructure/newcernuser.md)
* [Node monitoring](infrastructure/grafana.md)

### Common LCG tasks

* [Cleaning a workspace](tasks/cleanup.md)
* ~~[Install a new `gcc` compiler](tasks/installgcc.md)~~


